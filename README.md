# SMP-evo: Software Management Plans evolved

See [GitHub.com/SoftwareSaved/software-management-plans/issues/12](https://github.com/softwaresaved/software-management-plans/issues/12)
for the purpose of the project/repository and for ideas about it. It is currently
a **prototopy for testing only**, not ready for productive usage!

## How is this repo template structured?

- Each `section` from the [SSI's "SMP Checklist"](https://github.com/softwaresaved/software-management-plans/blob/master/data/checklist.yaml)
  is mapped as one issue label here.
- **Some** of the `question`s are mapped as issues here, with some `consider`ations and
  `guidance`s listed as `- [ ] action item`s.

## How to use start building your software according to a management plan?

1. Download the `.tar.gz` file from this repo.
1. Instead of starting a "blank" project, or creating it from a GitLab template,
   use [the `Import project > ... from > GitLab export` option](https://docs.gitlab.com/ce/user/project/settings/import_export.html#importing-the-project).
1. Select/Upload/Import the `.tar.gz` from before.
1. Start developing your project, taking the pre-defined issues into account, 
   for example by using [commit message keywords to auto-close them](https://docs.gitlab.com/ce/user/project/issues/automatic_issue_closing.html).
1. Replace this `README.md`'s content with that of your own project.

## Open questions (Feedback welcome! Please comment in [GitHub.com/softwaresaved/software-management-plans/issues/12](https://github.com/softwaresaved/software-management-plans/issues/12).)

- Is the current mapping (see above) logical/useful? If not, to which other GitLab
  features might `section`s, `question`s and `consider`ations be mapped?

## Known issues

- Works only on GitLab. If you know any way to import issues into a Git_Hub_ repo,
  please comment into [GitHub.com/softwaresaved/software-management-plans/issues/12](https://github.com/softwaresaved/software-management-plans/issues/12)!
- After importing, inter-issue links with the `#` are not re-linked automatically.
  To do that,  edit the description, click on the `#` and select the target issue.
- Project features (see `Settings > Permissions`) that were disabled before export
  are enabled again after import. May actually be a good thing, because the imported
  project is closer to GitHub's default.
- Not all of the SSI's `question`s, `consider`ations and `guidance`s have been
  transferred into this prototype.
- When a template issue here is deleted, it's ID number remains reserved in the
  project, and this reservation carries over through the export. Meaning that deleted
  ID numbers become and remain a gap in the imported project's issue list.

## License <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/3.0/80x15.png" /></a>

This `README.md` is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/">Creative Commons Attribution-NonCommercial 3.0 Unported License</a>.
See [`LICENSE.md`](LICENSE.md) for all other project content.
