# Copyright and Licence

Except for the `README.md`, this project consists of issue titles and descriptions derived from the
[SSI's `software-management-plans`](https://github.com/softwaresaved/software-management-plans).
Hence, their license applies.

## Advice and guidance text

Except where otherwise noted, advice and guidance text is licensed under a [Creative Commons Attribution Non-Commercial 2.5 License](http://creativecommons.org/licenses/by-nc/2.5/scotland/).

Copyright (c) 2014–2016 [The University of Edinburgh](http://www.ed.ac.uk) on behalf of [The Software Sustainability Institute](http://www.software.ac.uk).

The following is a human-readable summary of (and not a substitute for) the [full legal text of the Creative Commons Attribution Non-Commercial 2.5 License](http://creativecommons.org/licenses/by-nc/2.5/scotland/legalcode).

**You are free to:**

* **Share** — copy and redistribute the material in any medium or format
* **Adapt** — remix, transform, and build upon the material

The licensor cannot revoke these freedoms as long as you follow the license terms.

**Under the following terms:**

* **Attribution** — You must give appropriate credit (mentioning that your work is derived from work that is Copyright (c) The University of Edinburgh and, where practical, linking to http://www.software.ac.uk/), provide a [link to the license](http://creativecommons.org/licenses/by-nc/2.5/scotland/), and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* **Non-Commercial** — You may not use the material for commercial purposes.

**No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

**Notices:**

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.
